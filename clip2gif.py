from argparse import ArgumentParser, RawTextHelpFormatter, SUPPRESS
import imageio
import os
import sys

__version__ = '1.0'


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Clip2Gif converts any clip to a gif.

    If "-compression" is used, the number of frames captured can be reduced, making the output smaller.
    Compressing the output also means increase the speed.
    Default = No compression.


    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=SUPPRESS)
    # help='Increase output verbosity. Output files don\'t overwrite original ones'

    parser.add_argument("-c", "--compression", dest="compression", default=1, type=int,
                        help="Compression level to apply.")
    # action='store_true'
    # nargs="+",
    # nargs='?',  # optional argument
    # default=""
    # type=int
    # choices=[]

    parser.add_argument("clip",
                        help="Input clip to convert to gif")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


def gifMaker(inputPath, targetFormat, compression=0):
    outputPath = os.path.splitext(inputPath)[0] + targetFormat

    print('converting {} \n to {}'.format(inputPath, outputPath))

    reader = imageio.get_reader(inputPath)
    fps = reader.get_meta_data()['fps']

    writer = imageio.get_writer(outputPath, fps=fps)

    global_count = 0
    count = 0
    print("Converting...", end='\r')

    for frames in reader:
        if count == compression:
            count = -1  # reset count (-1 because later it's increased by 1)
            writer.append_data(frames)
            # print('Frame {}'.format(frames))

        global_count += 1
        count += 1

    print(' ' * 20, end='\r')
    print('Done')
    writer.close()


def main(args=None):
    if args is None:
        args = check_args()

    gifMaker(args.clip, '.gif', args.compression)


if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
